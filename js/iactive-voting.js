(function(){

    /**
     * Функция создания нового DOM объекта
     * @param  {Object} params параметры создаваемого объекта. Ключи совпадают с возможными ключами DOM объекта.
     *                         Можно передать ключ tag и будет создан указанный тег.
     *                         Можно передать события. События передаются с префиксом on (onclick, onsubmit)
     *                         Можно указать текст объекта. Указывается ключом text
     * @param  {DOM} parent родительский DOM объект
     * @return {DOM}        возвращает созданный объект
     */
    function newNode(params, parent) {
        var tag = params['tag'] || 'div';
        var newEl = document.createElement(tag);
        for (var paramName in params) {
            if ((paramName != 'tag') && (paramName != 'text')
                    && (!paramName.match(/^on\w+/))) {
                newEl[paramName] = params[paramName];
            }
            if (paramName.match(/^on(\w+)/)) {
                var eventName = RegExp.$1;
                attachEvent(newEl, eventName, params[paramName]);
            }
        }
        if (params['text'] != null) {
            newEl.appendChild(newTextNode(params['text']));
        }
        if (parent != null) {
            parent.appendChild(newEl);
        }
        return newEl;
    }

    /**
     * Создает текстовую ноду в DOM
     * @param  {String} string текст, который нужно добавить
     * @return {DOM}         Возвращает текстовую ноду
     */
    function newTextNode(string) {
        return document.createTextNode(string);
    }

    /**
     * Функция добавления css класса к тегу
     * @param {DOM} node      DOM объект, к которому нужно добавить класс
     * @param {String} className название css класса (можно несколько через пробел)
     */
    function addClass(node, className) {
        removeClass(node, className);
        node.className += ' ' + className;
    }

    /**
     * Функция удаления css класса у объекта
     * @param  {DOM} node      DOM объект, у которого нужно удалить класс
     * @param  {String} className строка с css классом, который нужно удалить (можно несколько через пробел)
     */
    function removeClass(node, className) {
        var classes = node.className.split(' ');
        for (var i = 0; i < classes.length; i++) {
            if (classes[i] == className) {
                classes.splice(i, 1);
            }
        }
        node.className = classes.join(' ');
    }

    /**
     * Функция добавления стилей к объекту
     * @param {DOM} node DOM объект к которому нужно добавить стиль
     * @param {Object} args js объект со стилями
     */
    function addStyle(node, args) {
        for (var arg in args) {
            if ((arg == 'left') || (arg == 'top') || (arg == 'width')
                    || (arg == 'height') || (arg == 'right') || (arg == 'bottom'))
                args[arg] += 'px';
            node.style[arg] = args[arg];
        }
    }

    /**
     * Функция добавления обработчика события к DOM объекту
     * @param  {DOM} object   DOM объект, на который нужно повесить обработчик
     * @param  {String} event    название события
     * @param  {Function} method   функция, которую нужно вызвать по указанному событию
     * @param  {String} methodId (опционально) id события
     */
    function attachEvent(object, event, method, methodId) {
        if (!methodId) {
            methodId = '';
        }
        object['event_' + event + methodId] = function(event) {
            method.apply(object, [event]);
        };
        if (typeof(object['addEventListener']) == 'function') {
            object.addEventListener(event, object['event_' + event + methodId],
                    false);
        } else {
            object.attachEvent('on' + event, object['event_' + event + methodId]);
        }
    }

    /**
     * Функция удаления обработчика события у DOM объекта
     * @param  {DOM} object   DOM объект, у которого нужно удалить обработчик события
     * @param  {String} event    название события
     * @param  {String} methodId (опционально) id события
     */
    function detachEvent(object, event, methodId) {
        if (!methodId) {
            methodId = '';
        }
        if (typeof(object['removeEventListener']) == 'function') {
            object.removeEventListener(event, object['event_' + event + methodId],
                    false);
        } else {
            object.detachEvent('on' + event, object['event_' + event + methodId]);
        }
    }

    /**
     * Функция вызова события на DOM объекте
     * @param  {DOM} el        DOM объект, на котором нужно вызвать событие
     * @param  {String} eventName название события
     * @param  {Object} options   параметры события
     */
    function triggerEvent(el, eventName, options) {
        var event;
        if (window.CustomEvent) {
            event = new CustomEvent(eventName, options);
        } else {
            event = document.createEvent('CustomEvent');
            event.initCustomEvent(eventName, true, true, options);
        }
        el.dispatchEvent(event);
    }

    /**
     * Функция сериализации формы
     * @param  {DOM} form   DOM объект формы, которую нужно сериализовать
     * @return {Array}     Массив объектов из полей формы. В массиве объекты содержат ключи name, value (название поля формы и значение соответственно)
     */
    function serializeForm(form) {
        var field, s = [];
        if (typeof form == 'object' && form.nodeName.toLowerCase() === 'form') {
            var len = form.elements.length;
            for (var i = 0; i < len; i++) {
                field = form.elements[i];
                if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
                    if (field.type == 'select-multiple') {
                        for (var j = form.elements[i].options.length-1; j >= 0; j--) {
                            if(field.options[j].selected)
                                s[s.length] = { name: field.name,
                                                value: field.options[j].value };
                        }
                    } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
                        s[s.length] = { name: field.name,
                                        value: field.value };
                    }
                }
            }
        }
        return s;
    }

    /* Ajax Loader */
    var CL = new Object();
    CL.READY_STATE_UNINITIALIZED = 0;
    CL.READY_STATE_LOADING = 1;
    CL.READY_STATE_LOADED = 2;
    CL.READY_STATE_INTERACTIVE = 3;
    CL.READY_STATE_COMPLETE = 4;

    CL.loader = function(url, method, requestParams) {
        this.url = url;
        this.requestParams = requestParams;
        this.method = method;
    };

    CL.loader.prototype = {
        getTransport : function() {
            var transport;
            if (window.XMLHttpRequest) {
                transport = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                try {
                    transport = new window.ActiveXObject('Msxml2.XMLHTTP');
                } catch (err) {
                    transport = new window.ActiveXObject('Microsoft.XMLHTTP');
                }
            }
            return transport;
        },
        sendRequest : function(success, fail, loading) {
            var request = this.getTransport();
            request.open(this.method, this.url, true);
            // request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            // request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            var oThis = this;
            request.onreadystatechange = function() {
                oThis.handleAjaxResponse(request, success, fail, loading);
            };
            request.send(this.queryString());
        },
        queryString : function(args) {
            var requestParams = [];
            for (var i = 0; i < this.requestParams.length; i++) {
                requestParams.push(this.requestParams[i]);
            }
            if(args){
                for (i = 0; i < args.length; i++) {
                    requestParams.push(args[i]);
                }
            }
            var queryString = '';
            if (requestParams && requestParams.length > 0) {
                for (i = 0; i < requestParams.length; i++) {
                    queryString += requestParams[i] + '&';
                }
                queryString = queryString.substring(0, queryString.length - 1);
            }
            return queryString;
        },
        handleAjaxResponse : function(request, success, fail, loading) {
            if (request.readyState === CL.READY_STATE_COMPLETE) {
                if (this.isSuccess(request)) {
                    if(typeof(success) === 'function'){
                        success(request);
                    }
                    // this.component.ajaxUpdate(request);
                } else {
                    if(typeof(fail) === 'function'){
                        fail(request);
                    }
                    // this.component.handleError(request);
                }
            }
            if (request.readyState === CL.READY_STATE_LOADING) {
                if (typeof(loading) == 'function') {
                    loading(request);
                }
            }
        },
        isSuccess : function(request) {
            return request.status === 0 || (request.status >= 200 && request.status < 300);
        }
    };

    CL.xmlSet = function(xmlElement) {
        var len = xmlElement.childNodes.length;
        for (var i = 0; i < len; i++) {
            var attrName = xmlElement.childNodes[i].tagName;
            var attrValue = CL.textContent(xmlElement.childNodes[i]);
            this[attrName] = attrValue;
        }
    };

    CL.jsonSet = function(json) {
        for(var attrName in json){
            this[attrName] = json[attrName];
        }
    };

    /**
     * Merge the contents of two or more objects together into the first object
     * (based on jQuery extend function)
     * @param {Boolean} [recursive] - recursive flag
     * @param {Object} target - target object
     * @param {Object*} [objectN] - object to merge in
     * @return {Object} target object
     */
    Object.extend = function() {
        var n = 1,
            recursive = false,
            target = arguments[0],
            obj,
            name, src, val;

        // check for recursive flag in first argument
        if (typeof target === 'boolean') {
            recursive = target;
            target = arguments[1];
            n = 2;
        }

        if ((typeof target !== 'object') && (typeof target !== 'function')) {
            target = {};
        }

        for ( ; n < arguments.length; n++) {
            if ((obj = arguments[n]) != null) {
                for (name in obj) {
                    src = target[name];
                    val = obj[name];
                    // convert Number and String object to primitives
                    if (typeof val === 'object') {
                        if (val instanceof Number) {
                            val = Number(val);
                        } else if (val instanceof String) {
                            val = String(val);
                        }
                    }
                    // Try to recursive merge if copy is object and nor array or function
                    if ((recursive) && (typeof val === 'object') && (val) && (!(val instanceof Function)) && (!(val instanceof Array))) {
                        target[name] = Object.extend(recursive, src, val);
                    } else if (val !== undefined) {
                        target[name] = val;
                    }
                }
            }
        }
        return target;
    };

    /**
     * Merging content two or more objects non-recursive to new object
     * @param {Object*} two or more objects
     * @return {Object} the result
     */
    Object.merge = function() {
        var args = [{}];
        for (var i = 0; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        return Object.extend.apply(this, args);
    };

    /**
     * Функция инициализации голосования
     * @param  {Object} options объект параметров голосования
     * Параметры голосования
     * @param  {String} blockId  строка с id DOM блока, куда будет выведено голосование
     * @param  {String} votingId id голосования на стороне сервиса
     * @param  {String} votingHost адрес хоста, откуда получаем голосование. По умолчанию http://iactive.pro
     * @param  {String} title заголовок блока. По умолчанию "Опрос"
     * @param  {String} button надпись на кнопке отправки голосования. По умолчанию 'Проголосовать',
     * @param  {String} css объект с настройками CSS
        * @param  {String} base база (префикс) для сборки названия класса. По умолчанию 'iactive'. С указанным параметром классы формируются в виде BASE__CLASS
            * @param  {Object} classes объект со списком css классов
                * @param  {String} wrap класс для всего блока голосования. По умолчанию 'block'
                * @param  {String} title класс для заголовка блока голосования. По умолчанию 'title',
                * @param  {String} question класс для вопроса голосования. По умолчанию  'question',
                * @param  {String} answer класс для блока вопроса голосования. По умолчанию  'answer',
                * @param  {String} phone класс для блока телефона голосования. По умолчанию  'phone',
                * @param  {String} line класс для блока линии процентов голосования. По умолчанию  'line',
                * @param  {String} percent класс для блока самих процентов голосования. По умолчанию  'percent',
                * @param  {String} submit класс для блока отправки голосования. По умолчанию  'submit',
                * @param  {String} button класс для кнопки голосования. По умолчанию  'button'
            * @param {Object} modifiers - объект модификаторов классов
                * @param  {String} result класс модификатор, для случая когда голосование отображает результаты
     */
    function iActiveVoting(options){
        var opt = Object.extend(true, {
            blockId: 'iactive-voting',
            votingId: '31channel351',
            votingHost: 'http://iactive.pro',
            title: 'Опрос',
            button: 'Проголосовать',
            css: {
                base: 'iactive',
                classes: {
                    wrap: 'block',
                    title: 'title',
                    question: 'question',
                    answer: 'answer',
                    phone: 'phone',
                    line: 'line',
                    percent: 'percent',
                    submit: 'submit',
                    button: 'button'
                },
                modifiers: {
                    result: 'm--result'
                }
            }
        }, options);

        var baseUrl = '';
        var votingUrl = '';
        var loader = '';

        var votingBlock = document.getElementById(opt.blockId);
        if(!votingBlock){
            console.log('Блок с id ' + opt.blockId + ' для вывода голосования не найден.');
        } else {
            baseUrl = _constructBaseUrl();
            votingUrl = _consructVotingUrl();
            loader = new CL.loader('', 'GET', []);
            getVoting();
        }

        function getVoting(){
            loader.url = votingUrl;
            loader.sendRequest(initVoting, errorRequest);
        }

        function initVoting(request){
            if(request.responseText){
                var voting = JSON.parse(request.responseText);
                if(typeof(voting) === 'object'){
                    renderBlock(voting);
                }
            }
        }

        function sendAnswer(form){
            var data = serializeForm(form);
            if(data.length){
                loader.url = _consructVotingPollUrl(data[0].value);
                loader.sendRequest(function(){
                    getVoting();
                }, errorRequest);
            }
        }

        function renderBlock(voting){
            votingBlock.innerHTML = '';
            if(voting.answers){
                var parent = undefined;
                var fragment = document.createDocumentFragment();
                var classWrap = getClass('wrap');
                var classWrap__mod = '';
                if(voting.show_results == '1'){
                    parent = newNode({}, fragment);
                    classWrap__mod = getClass('result', true);
                } else {
                    parent = newNode({
                        tag: 'form',
                        action: '#'
                    }, fragment);
                }

                if(classWrap__mod !== ''){
                    classWrap+=' '+classWrap__mod;
                }
                addClass(parent, classWrap);

                if(opt.title){
                    newNode({
                        className: getClass('title'),
                        text: opt.title
                    }, parent);
                }

                var question = newNode({
                    className: getClass('question')
                }, parent);

                question.appendChild(newTextNode(voting.question));

                if(voting.show_results == '1'){
                    var percent = 100;
                    var i = 0;
                    var max_value = 0;
                    var max_idx = -1;
                    while(i < voting.answers.length){
                        var p = voting.answers[i].percent;
                        percent = percent - p;
                        if((voting.answers[i].percent >= max_value) && (voting.answers[i].percent > 0)){
                            max_value = voting.answers[i].percent;
                            max_idx = i;
                        }
                        i++;
                    }
                    if((percent != 0) && (max_idx > 0)){
                        voting.answers[max_idx].percent+= percent;
                    }
                }

                i = 0;
                while(i < voting.answers.length){
                    var item = newNode({
                        tag: 'label',
                        className: getClass('answer')
                    }, parent);

                    if(voting.show_results != '1'){
                        newNode({
                            tag: 'input',
                            type: 'radio',
                            name: 'answer',
                            value: i + 1
                        }, item);
                    }

                    item.appendChild(newTextNode(voting.answers[i].answer));

                    if(voting.answers[i].phone !== ''){
                        newNode({
                            tag: 'span',
                            className: getClass('phone'),
                            text: voting.answers[i].phone
                        }, item);
                    }

                    if(voting.show_results == '1'){
                        p = voting.answers[i].percent;
                        var line = newNode({
                            tag: 'span',
                            text: voting.answers[i].count + ' (' + p + '%)',
                            className: getClass('percent')
                        }, newNode({
                            className: getClass('line')
                        }, item));

                        addStyle(line, {
                            minWidth: p + '%'
                        });
                    }

                    i++;
                }

                if(voting.show_results != '1'){
                    var submit = newNode({
                        className: getClass('submit')
                    }, parent);

                    newNode({
                        tag: 'button',
                        type: 'submit',
                        text: opt.button,
                        className: getClass('button')
                    }, submit);

                    attachEvent(parent, 'submit', function(e){
                        e.preventDefault();

                        sendAnswer(this);
                    });
                }

                votingBlock.appendChild(fragment);
            }
        }

        /**
         * Функция получения класса
         * @param  {String}  name        название ключа класса, который нужно получить
         * @param  {Boolean} is_modifier является ли класс модификатором
         * @return {String}              строка итогового класса
         */
        function getClass(name, is_modifier){
            var cl = '';
            if(is_modifier) {
                if(opt.css.modifiers[name]){
                    cl = opt.css.modifiers[name];
                }
            } else {
                if(opt.css.classes[name]){
                    cl = opt.css.classes[name];
                    if(opt.css.base){
                        cl = opt.css.base + '__' + cl;
                    }
                }
            }
            return cl;
        }

        function errorRequest(){
            console.log('Ошибка во время получения голосования. Проверьте параметры');
        }

        /**
         * Функция сборки базового урла для сервиса
         * @return {String} Строку с базовым урлом сервиса
         */
        function _constructBaseUrl(){
            var url = '';
            if(opt.baseUrl){
                url = opt.baseUrl;
            } else {
                url = opt.votingHost + '/' + opt.votingId;
            }
            return url;
        }

        /**
         * Функция сборки урла для получения голосования
         * @return {String} Строку с урлом голосования
         */
        function _consructVotingUrl(){
            var url = '';
            if(opt.votingUrl){
                url = baseUrl + '/' + opt.votingUrl;
            } else {
                url = baseUrl + '/voiting_' + opt.votingId + '.php';
            }
            url+='?type=4';

            return url;
        }

        /**
         * Функция сборки урла для отправки голоса
         * @return {String} Строку с урлом отправки голоса
         */
        function _consructVotingPollUrl(id){
            var url = baseUrl + '/poll_var' + id + '_' + opt.votingId +'.php';
            return url;
        }
    }

    window.iActiveVoting = iActiveVoting;

})();
